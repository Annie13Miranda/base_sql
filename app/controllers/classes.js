module.exports = function(db){
    const express = require('express');
    const router = express.Router(); //go to direction /
    let TABLE = 'class';

    //{{SERVER}}/classs/create

    router.get('/create',function(request,response){ //method where create classs if not exist
        db.serialize(function(){
            db.run('CREATE TABLE IF NOT EXISTS '+TABLE+' (name TEXT,id_class INT,room TEXT)'); //create table with name, id_class and room
            response.send('Created');
        });
    });
    
    //{{SERVER}}/classs/clear

    router.get('/clear',function(request,response){ //clear all classs of database
        db.serialize(function(){
            db.run('DROP TABLE IF EXISTS '+TABLE); 
            response.send("Cleared");       
        });
    });
    
    //{{SERVER}}/classs/show

    router.get('/show',function(request,response){ //method for show all classs
        db.serialize(function(){
            db.all('SELECT * FROM '+TABLE+'',function(error,rows){
                if (error) {
                    response.send(error);
                } else {
                    response.send(rows);
                }
            });
        });
    });

    //{{SERVER}}/classs/id

    router.get('/:id',function(request,response){ //method for show one class since id
        let id = request.params.id;
        db.serialize(function(){
            db.all('SELECT * FROM '+TABLE+' WHERE id_class= '+id,function(error,rows){
                if (error) {
                    response.send(error);
                } else {
                    response.send(rows[0]);
                }
            });
        });
    });
    
    //{{SERVER}}/classs/insert

    router.post('/insert',function(request,response){ //insert classs of body
        db.serialize(function(){
            db.run('INSERT INTO '+TABLE+' values ("'
            +request.body.name+'",'
            +request.body.id_class+',"'
            +request.body.room+'")');
            response.send('class add with name:'+request.body.name +','+request.body.id_class+','+request.body.room);
        });
    });

    //{{SERVER}}/classs/update

    router.put('/update',function(request,response){ //insert classs of body
        db.serialize(function(){         
            db.run("UPDATE '"+TABLE+"' SET name='"+request.body.name+"', room='"+request.body.room+"' WHERE id_class="+request.body.id_class);
            response.send('Update class:'+request.body.id_class+' with the information: '+request.body.room+','+request.body.name);
        });
    });
    
    //{{SERVER}}/classs/id

    router.delete('/:id',function(request,response){ //method for delete one class
        let id = request.params.id;
        db.serialize(function(){
            db.all('DELETE FROM '+TABLE+' WHERE id_class= '+id,function(error,rows){
                if (error) {
                    response.send(error);
                } else {
                    response.send("class delete");
                }
            });
        });
    });
   
       
    return router;  
}