module.exports = function(db){
    const express = require('express');
    const router = express.Router(); //go to direction /
    let TABLE = 'users';

    //{{SERVER}}/users/create

    router.get('/create',function(request,response){ //method where create users if not exist
        db.serialize(function(){
            db.run('CREATE TABLE IF NOT EXISTS '+TABLE+' (name TEXT,id_user INT,email TEXT)'); //create table with name, id_user and email
            response.send('Created');
        });
    });
    
    //{{SERVER}}/users/clear

    router.get('/clear',function(request,response){ //clear all users of database
        db.serialize(function(){
            db.run('DROP TABLE IF EXISTS '+TABLE); 
            response.send("Cleared");       
        });
    });
    
    //{{SERVER}}/users/show

    router.get('/show',function(request,response){ //method for show all users
        db.serialize(function(){
            db.all('SELECT * FROM '+TABLE+'',function(error,rows){
                if (error) {
                    response.send(error);
                } else {
                    response.send(rows);
                }
            });
        });
    });

    //{{SERVER}}/users/id

    router.get('/:id',function(request,response){ //method for show one user since id
        let id = request.params.id;
        db.serialize(function(){
            db.all('SELECT * FROM '+TABLE+' WHERE id_user= '+id,function(error,rows){
                if (error) {
                    response.send(error);
                } else {
                    response.send(rows[0]);
                }
            });
        });
    });
    
    //{{SERVER}}/users/insert

    router.post('/insert',function(request,response){ //insert users of body
        db.serialize(function(){
            db.run('INSERT INTO '+TABLE+' values ("'
            +request.body.name+'",'
            +request.body.id_user+',"'
            +request.body.email+'")');
            response.send('User add with name:'+request.body.name +','+request.body.id_user+','+request.body.email);
        });
    });

    //{{SERVER}}/users/update

    router.put('/update',function(request,response){ //insert users of body
        db.serialize(function(){         
            db.run("UPDATE '"+TABLE+"' SET name='"+request.body.name+"', email='"+request.body.email+"' WHERE id_user="+request.body.id_user);
            response.send('Update user:'+request.body.id_user+' with the information: '+request.body.email+','+request.body.name);
        });
    });
    
    //{{SERVER}}/users/id

    router.delete('/:id',function(request,response){ //method for delete one user
        let id = request.params.id;
        db.serialize(function(){
            db.all('DELETE FROM '+TABLE+' WHERE id_user= '+id,function(error,rows){
                if (error) {
                    response.send(error);
                } else {
                    response.send("User delete");
                }
            });
        });
    });
   
       
    return router;  
}